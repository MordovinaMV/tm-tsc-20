package ru.tsc.mordovina.tm.exception.system;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error. Access denied.");
    }

}
