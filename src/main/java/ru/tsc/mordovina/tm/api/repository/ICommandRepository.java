package ru.tsc.mordovina.tm.api.repository;

import ru.tsc.mordovina.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getCommands();

    Collection<AbstractCommand> getArguments();

    AbstractCommand getCommandByName(String name);

    AbstractCommand getCommandByArg(String arg);

    Collection<String> getCommandNames();

    Collection<String> getArgNames();

    void add(AbstractCommand command);

}
